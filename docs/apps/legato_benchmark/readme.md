﻿---
parent: Example Applications
title: Legato Benchmark
nav_order: 3
---

# Legato Benchmark

![](./../../images/legato_bm_wqvga_run1.png)

This application shows the frame update rates for various operations in the Legato Graphics Library, including string rendering, area fills and image decode and rendering. The benchmarks can be configured for different text sizes, number and size of discrete area fills and different image formats. The instantaneous and averaged frame update rates are shown at real-time on the demo.

This demonstration runs on:

|MPLABX Configuration|Board Configuration|
|:-------------------|:------------------|
|[legato_bm_ck_cu_wqvga.X](./firmware/legato_bm_ck_cu_wqvga.X/readme.md)|PIC32CK-GC Curiosity Ultra using LCC MCU graphics controller to drive the [High-Performance WQVGA Display Module with maXTouch® Technology](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/AC320005-4)|


 
