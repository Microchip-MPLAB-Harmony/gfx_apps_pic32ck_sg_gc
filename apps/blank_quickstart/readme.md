﻿---
parent: Example Applications
title: Blank Quickstart
nav_order: 10
---

# Blank Quickstart

![](./../../images/blank_quickstart.png)

This demonstration provides a starting point to integrate a third-party graphics library with MPLAB Harmony Graphics Suite.

|MPLABX Configuration|Board Configuration|
|:-------------------|:------------------|
|[blank\_qs\_ck\_cu\_wqvga.X](./firmware/blank_qs_ck_cu_wqvga.X/readme.md)|PIC32CK_GC Curiosity Ultra using LCC MCU DMA graphics controller to drive the [High-Performance WQVGA Display Module with maXTouch® Technology](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/AC320005-4)|